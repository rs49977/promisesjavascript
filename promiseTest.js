const fs = require('fs'); 
const { resolve } = require("path");

// fs readFile method using with promise
function readFile(filePath){
    return new Promise((resolve,reject) => {
        fs.readFile(filePath,'utf-8',(err,data) => {
            if(err) reject(err)
            else resolve(data)
        }) 

    })
    
}   

readFile('sample.txt')
.then((data) => console.log(data.length))
.catch((err) => console.log(err))

// inbuild promise method for fs readFile
fs.promises.readFile('sample.txt','utf-8')
.then((data) => console.log(data.length))
.catch((err) => console.log(err))


let content = "Aeque enim contingit omnibus fidibus, ut incontentae sint."
//  fs writeFile method using with promise
function writeFile(filePath){
    return new Promise((resolve,reject) => {
        fs.writeFile(filePath,content,'utf-8',(err) => {
            if(err) reject(err)
            else resolve("File written successfuly!")
        }) 

    })
    
}   

writeFile('sample1.txt')
.then((msg) => console.log(msg))
.catch((err) => console.log(err))

// inbuild promise method for fs writeFile
fs.promises.writeFile('sample2.txt',content,'utf-8')
.then(() => console.log("File written successfuly!"))
.catch((err) => console.log(err))


function accessFile(filePath){
    return new Promise((resolve,reject) => {
        fs.writeFile(filePath,'utf-8',(err) => {
            if(err) reject(err)
            else resolve("File created successfully")
        }) 

    })
    
}   

accessFile('sample3.txt')
.then((msg) => console.log(msg))
.catch((err) => console.log(err))

